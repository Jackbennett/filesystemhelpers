function New-Directory {
    <#
    .SYNOPSIS
        Make a new folder and open it. Alias of: mkdir <name>; cd <name>

        mkdir returns the directory so you could of piped that example: mkdir <name> | cd
    .DESCRIPTION
        Pushes location on the stack rather that Set-location jsut incase you want to pop-location to undo
    #>
    param (
        [string]$name
    )
    New-Item -ItemType Directory -Name $name | Push-Location
}

class DisplayArray {
    [string]$Key
    [string]$Name

    DisplayArray([string]$Name) {
        switch -regex ($name) {
            "^(\d+)" { $this.Key = [int]$matches[1]; Break } # Any integer Key
            "^[a-z]" { $this.Key = $Name.ToUpper()[0]; Break } # Capitalize a letter Key
            Default { $this.Key = $Name[0]; Break } # anything else, e.g. dotfiles all under .
        }
        $this.Name = $name
    }
}

function Format-Directory {
    param (
        $Path = $pwd,
        [UInt16]$Width = 2048,
        [switch]$IncludeFiles
    )
    # Goal:
    # (Get-ChildItem $path).forEach( { [pscustomobject]@{"letter" = $psitem.name[0]; "name" = $psitem.name; "object" = $psitem } }) |
    #     Format-Wide -Property name -GroupBy letter -AutoSize

    $directory = -not $IncludeFiles # invert files switch A. Can't alias parameters to a funtion and B. [switch] shouldn't default $true. This is clearer to read.
    # $list = (Get-ChildItem $path -Directory:$Directory).forEach( { [DisplayArray]::new($psitem.name) })
    $list = (Get-ChildItem $Path -Directory:$Directory).forEach( { [DisplayArray]::new($psitem.name) })


    [System.Collections.Generic.Dictionary[System.String, System.Collections.Generic.List[String]]]$groups = [System.Collections.Generic.Dictionary[System.String, System.Collections.Generic.List[String]]]::new()
    $list.forEach({
        if (-not $groups.tryAdd($psitem.Key, $psitem.Name)){
            $groups[$psitem.Key].add($psitem.Name)
        }
    })

    # Pipe through format|out-string to force wrapping output rather than {propert...} shortening
    $groups | format-table -Property * -AutoSize | Out-String -Width $Width
}

function Show-DirectoryContents{
    <#
    .SYNOPSIS
        wrap ls or cat. See: help -examples Show-DirectoryContents
    .DESCRIPTION
        Ever `ls <file>` or `cat <directory>`? This will call the right one for you.
        If there's only one file in a folder then we cat it. If there's only 1 folder, we'll go into it and check again.

        Suggested to alias as lc as short for list-contents
    .EXAMPLE
        c:\src > lc .\streamer\
        lc will now get-contents(cat) the only (example)file in the folder while changing directory to it (push-location)

        FROM balenalib/raspberry-pi-debian:buster as build

        RUN curl http://www.linux-projects.org/listing/uv4l_repo/lpkey.asc | sudo apt-key add -
        RUN echo "deb http://www.linux-projects.org/listing/uv4l_repo/raspbian/stretch stretch main" | sudo tee -a /etc/apt/sources.list


        RUN apt-get update && \
            apt-get install -y --no-install-recommends \
            uv4l \
            uv4l-server \
            uv4l-uvc \
            uv4l-xscreen \
            uv4l-mjpegstream \
            uv4l-dummy \
            uv4l-raspidisp \
            uv4l-webrtc \
            uv4l-demos \
            uv4l-raspicam && \
            apt-get clean && \
            rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

        EXPOSE 8080

        CMD ["uv4l", "--foreground","--auto-video_nr", "--sched-rr", "--driver", "raspicam", "--enable-server=auto", "--server-option", "--port=8080"]
        C:\src\streamer > _
    .INPUTS
        System.String
    .OUTPUTS
        System.IO.FileInfo, System.String
    #>
    Param(
        [Parameter(Position=0)]
        [string]$Path = $pwd
    )
    if(test-path $Path){
        switch ((get-item $Path).GetType().name) {
            'FileInfo' {Get-Content $Path}
            'DirectoryInfo' {
                $result = Get-ChildItem $Path
                if($result.count -eq 1){
                    Show-DirectoryContents $result
                } else {
                    $result
                }
            }
            default {
                Write-Error "Not sure to ls or cat"
            }
        }
    } else {
        Write-Error "Invalid $Path"
    }
}
