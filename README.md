Simple tools directly useful working with folders. In general see each function help output.

![Example of format-directory and new directory](./docs/example-lsa-new.png)

I don't export these alias' from the module they're too generic to assume you'd want them. Alias it in your profile like I do.

# lsa / Format-Directory

Pretty-print the current folder, I use it as a sort of project overview tool since all my work is organized at a root `src` folder.

It used to just format-wide but I like sorting it by letter.

Remembered by "**L**ist **S**ystem **All**"... What does `ls` actually stand for?

# new / New-Directory

wraps `mkdir | cd`

Remembered by "**New** \<project\>"

# lc / Show-DirectoryContents
`ls` or `cat`? I don't care just do it for me.

Remembered by "**L**ist **C**ontents"